<?php if ( !defined ('BASEPATH')) exit ('No direct script access allowed');

// author: Dennis Bond, Michigan State University (bonddenn@msu.edu)
// August 9, 2011

$plugin_info = array (
	'pi_name'			=> 'ANR Bar Element Loader',
	'pi_version'		=> '1.1',
	'pi_author' 		=> 'Dennis Bond',
	'pi_description' 	=> 'Provides the code needed to include the ANR Bar Element on a website'
);

/**
 * Generates ANR bar markup.
 */
class Anr_bar
{
	/**
	 * Data output by the plugin.
	 * 
	 * @var string
	 */
	public $return_data = 'fail';


	/**
	 * Loads the ANR bar view.
	 */
	public function __construct ()
	{
		// Get parameters
		$tab_list 			= '"'.implode('","', explode(',', get_instance()->TMPL->fetch_param('tabs'))).'"'; // Wraps all comma separated tag names in double quotes for JS array.
		$last_tab_name 		= get_instance()->TMPL->fetch_param('last_tab_name');
		$last_tab_url 		= get_instance()->TMPL->fetch_param('last_tab_url');
		$search_results_url = get_instance()->TMPL->fetch_param('search_results_url');

		// Load view
		$view_file_path = dirname(__FILE__).'/views/anr_bar.html';
		if ( file_exists($view_file_path) )
		{
			$this->return_data = get_instance()->TMPL->parse_variables(
				file_get_contents($view_file_path),
				array( array(
					'tab_list' 				=> $tab_list,
					'last_tab_name' 		=> $last_tab_name,
					'last_tab_url' 			=> $last_tab_url,
					'search_results_url' 	=> $search_results_url
				))
			);
		}
	}
}

/* End of file pi.anr_bar.php */
/* Location: ./third_party/anr_bar/pi.anr_bar.php */
